import { Component, OnInit } from '@angular/core';
import {NavItem} from '../../../models/VMmenu';
import {observable} from 'rxjs';
import {MenuDataService} from '../../../services/menu-data.service';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  isDisabled: boolean = true;
  navItems: NavItem[];
  constructor(private _menuservice:MenuDataService) { }

  ngOnInit() {
    
    this._menuservice.getMenuDataJson().subscribe(
      (data)=>{
        this.navItems = data;
      }
    )
    
  }

}
