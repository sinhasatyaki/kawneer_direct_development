import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  text: string;
  results: string[];
  searchCategory: any[];
  constructor() { }

  ngOnInit() {
    this.searchCategory = [
      { label: 'All Categories', value: null },
      { label: 'A3 - Closers', value: { id: 1, name: 'New York', code: 'NY' } },
      { label: 'A18 - Service Parts', value: { id: 2, name: 'Rome', code: 'RM' } },
      { label: 'LC01 - Trifab 400', value: { id: 3, name: 'London', code: 'LDN' } },

    ];
  }

}
