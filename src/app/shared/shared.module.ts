import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DropdownModule} from 'primeng/dropdown';
import { HeaderComponent } from './components/common-layout/header/header.component';
import { FooterComponent } from './components/common-layout/footer/footer.component';
import { MenuComponent } from './components/common-layout/menu/menu.component';
import {MegaMenuModule} from 'primeng/megamenu';

@NgModule({
  declarations: [HeaderComponent, FooterComponent, MenuComponent],
  imports: [
    CommonModule,
    DropdownModule,
    MegaMenuModule
  ],
  exports: [HeaderComponent,
            FooterComponent,
            MenuComponent
            
            ]
})
export class SharedModule { }
