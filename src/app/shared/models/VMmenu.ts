export interface NavItem{

    DisplayName:string;
    Children ?:NavItem[],
    RouterLink ?:string,
    Disabled ?: boolean
}