import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NavItem} from '../models/VMmenu'

@Injectable({
  providedIn: 'root'
})
export class MenuDataService {

  constructor(private _httpclient: HttpClient) { }

  getMenuDataJson(){
    return this._httpclient.get<NavItem[]>('./assets/menu-data.json').pipe(
           
    );
  }
}
